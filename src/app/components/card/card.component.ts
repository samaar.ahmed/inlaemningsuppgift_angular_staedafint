import { Component, OnInit } from '@angular/core';
import { Bookings } from 'src/app/bookings';
import { HttpClient } from '@angular/common/http';
import {FormArray,FormControl,FormGroup,NgForm,Validators} from '@angular/forms'


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit{
  
  bookingForm ! : FormGroup;

  doneBookingList : Bookings[] = []
  pendingBookingList :Bookings[] = []

  constructor(private http:HttpClient){}
  ngOnInit(){
    this.fetchTodo()
    this.bookingForm = new FormGroup({
      type : new FormControl(null,Validators.required),
      date : new FormControl(null,Validators.required),
      time : new FormControl(null,Validators.required),
      cleaner : new FormControl(null,Validators.required)
    })
    
  }
  
  bookingSort (a:Bookings,b:Bookings){

    const date1 = new Date(a.date)
    const date2 = new Date(b.date)
    if(date1>date2) return 1
    else if(date1<date2) return -1
    else return 0
  }
  fetchTodo(){
    this.http.get<Bookings[]>('./assets/bookings.json')
    .subscribe((res : Bookings[])=>{
      this.doneBookingList = res.filter(n=>n.status === true)
      this.pendingBookingList = res.filter(n=>n.status === false)
      this.pendingBookingList.sort((this.bookingSort))
      this.doneBookingList.sort((this.bookingSort))
      console.log(this.pendingBookingList)
      console.log(this.doneBookingList)
    })
  }

  booking : Bookings = {
    id: Math.random(), 
    date: '',
    time: '',
    customer: 'Anders',
    cleaner: '',
    type: '',
    status: false,
  }
  addBooking(object:any){
  this.booking = <Bookings>{id:Math.random(), date:object.date,time:object.time,customer:'Anders',cleaner:object.cleaner,type:object.type,status:false}
   if( this.pendingBookingList.find(item=>item.date === object.date))
     {
      console.log("Bokning finns redan med samma datum")
    }
    else if 
    (this.bookingForm.valid){
      this.pendingBookingList = [...this.pendingBookingList,this.booking]

    this.pendingBookingList.sort((this.bookingSort))
    }
   console.log(this.pendingBookingList)
    }
  



  }
  
  
  


