import { Component,Input} from '@angular/core';
import { Bookings } from 'src/app/bookings';


@Component({
  selector: 'app-bookinglist',
  templateUrl: './bookinglist.component.html',
  styleUrls: ['./bookinglist.component.css']
})
export class BookinglistComponent {

  @Input() li !: Bookings[];

  

  deleteItem(element: any) {
    this.li.forEach((value,index)=>{
      if(value == element)
      this.li.splice(index,1)
      console.log("Booking deleted")
    })

  }
  
}
