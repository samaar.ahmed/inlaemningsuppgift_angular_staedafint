import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import{HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonComponent } from './components/button/button.component';
import { BookinglistComponent } from './components/bookinglist/bookinglist.component';
import { CardComponent } from './components/card/card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import{MatIconModule} from '@angular/material/icon'
import {MatCardModule} from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListViewModule } from '@syncfusion/ej2-angular-lists';
import {RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component'

const rout : Routes = [
  {path:'',redirectTo:'home',pathMatch:'full'},
  {path:'home', component:HomeComponent},
  {path :'mina-sidor',component:CardComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    BookinglistComponent,
    CardComponent,
    HomeComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    ListViewModule,
    MatButtonModule,
    MatCardModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatIconModule,
    RouterModule.forRoot(rout)

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
